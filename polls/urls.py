from django.urls import path
from . import views

urlpatterns = [
    path('addBook', views.addBook, name='addBook'),
    path('getBooks',views.getBooks),
    path('getUpdateDeleteEmployee/<int:pk>',views.getUpdateDeleteEmployee)
]