from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import book
from .serializers import bookSerializer


@api_view(['POST'])
def addBook(request):
    data = {'bookName':request.data['bookName']}
    ser = bookSerializer(data = data )
    if ser.is_valid():
        pass
        ser.save()
        return Response(ser.data,status=status.HTTP_201_CREATED)
    else:
        return Response(ser.errors,status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def getBooks(request):
    books = book.objects.all()
    ser = bookSerializer(books,many=True)
    return Response(ser.data,status=status.HTTP_200_OK)

@api_view(['GET','PUT','DELETE'])
def getUpdateDeleteEmployee(request,pk):
    try:
        booKs = book.objects.get(pk=pk)
        if request.method =='GET':
            ser = bookSerializer(booKs)
            return Response(ser.data,status=status.HTTP_200_OK)

        elif request.method =='PUT':
            ser = bookSerializer(booKs,data=request.data)
            if ser.is_valid():
                ser.save()
                return Response(ser.data,status=status.HTTP_200_OK)
            else:
                return Response(ser.errors,status=status.HTTP_400_BAD_REQUEST)
            
        elif request.method =='DELETE':
            booKs.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
      
    except:
        return Response({'error':'not found'},status=status.HTTP_404_NOT_FOUND)


